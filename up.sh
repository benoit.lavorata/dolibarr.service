#!/bin/bash
echo ""
echo "Dolibarr + Mysql"
echo "Maintainer: Benoit Lavorata"
echo "License: MIT"
echo "==="
echo ""

FILE=".env"
if [ -f $FILE ]; then
    echo ""
    echo "File $FILE exists, will now start: ..."
    export $(cat .env | xargs)

    echo ""
    echo "Create networks ..."
    docker network create $APP_CONTAINER_NETWORK
    docker network create $DB_CONTAINER_NETWORK

    echo ""
    echo "Create volumes ..."
    docker volume create --name ${SERVICE_NAME}_${DB_CONTAINER_VOLUME_DATA}
    docker volume create --name ${SERVICE_NAME}_${APP_CONTAINER_VOLUME_DOCUMENTS}
    docker volume create --name ${SERVICE_NAME}_${APP_CONTAINER_VOLUME_HTML}
    docker volume create --name ${SERVICE_NAME}_${APP_CONTAINER_VOLUME_SCRIPTS}
    
    echo ""
    echo "Boot ..."
    docker-compose up -d --remove-orphans $1
    echo " "
    echo "Service should be available at this address: "${APP_CONTAINER_VHOST}${SERVICE_DOMAIN}
else
    echo "File $FILE does not exist: deploying for first time"
    cp .env.template .env

    echo "" >> .env
    echo "TARGET_UID=$UID" >> .env
    
    echo -e "Make sure to modify .env according to your needs, then use ./up.sh again."
fi

