#!/bin/bash
export $(cat .env | xargs)

echo "******"
echo "DANGER"
echo "******"
echo " "

echo " "
echo "******"
echo "This will delete the container an all related volumes + host binds. It is not reversible."
echo "This action can lead to data loss, only perform if you are sure."
echo " "
echo "The following data will be deleted if you continue:"
echo " - Volume: ${SERVICE_NAME}_${APP_CONTAINER_VOLUME_SCRIPTS}"
echo " - Volume: ${SERVICE_NAME}_${APP_CONTAINER_VOLUME_DOCUMENTS}"
echo " - Volume: ${SERVICE_NAME}_${APP_CONTAINER_VOLUME_HTML}"
echo " - Volume: ${SERVICE_NAME}_${DB_CONTAINER_VOLUME_DATA}"
echo "******"
echo " "

read -p "Are you sure (y/n)? " -n 1 -r
echo " "
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    read -p "Please reconfirm that you are sure (y/n)? " -n 1 -r
    echo " "
    echo    # (optional) move to a new line
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        # do dangerous stuff
        echo "OK, launching nuke"
        ./down.sh
        rm -rf $ODOO_CONTAINER_HOST_BIND
        docker volume rm ${SERVICE_NAME}_${APP_CONTAINER_VOLUME_SCRIPTS}
        docker volume rm ${SERVICE_NAME}_${APP_CONTAINER_VOLUME_DOCUMENTS}
        docker volume rm ${SERVICE_NAME}_${APP_CONTAINER_VOLUME_HTML}
        docker volume rm ${SERVICE_NAME}_${DB_CONTAINER_VOLUME_DATA}
    else
        echo "Nuke CANCELLED"
    fi
else
    echo "Nuke CANCELLED"
fi
